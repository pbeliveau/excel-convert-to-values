# Convert to values specific cells in Excel

![](/img/example.gif)

This code purpose is to find a specific string provided by the user in each
cells of a workbook and convert to result of the cell into static values. It has
the effect of doing a copy-paste as values systematicaly cells with a certain
string in them.

# Why you should use it

- You need to keep a static record of a workbook for future reference and you
  don't want to do a "Select all, copy, paste as values" since some cells needs
  to stay as formluas;
- Remove links to other workbooks;
- Just generaly faster than doing a simple copy-paste as values.

# How to use it

1. Open the Excel workbook you want to use this code on;
2. Press ALT+F11 to open the Visual Basic Editor in Excel;
3. In the menu, select "Insert" and "Module";
4. Copy-paste the content of the file "convert2values.bas" to the module in Excel;
5. From the Visual Basic Editor, select the module, press F5 to use it.