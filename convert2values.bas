' Philippe Beliveau
' MIT Licence
'
' Look for specific string in each
' cells  in each sheet and convert to
' values the result of the formula.
'
' This is basicaly an efficient way
' of copying-paste to values certain 
' cells only if they contain the specified
' string.
'
' VERSION 1.0
'
' Release history :
'      Version 1.0,    June 07, 2018  ~   Initial release

Option Explicit
Dim sht         As Worksheet
Dim rg          As Range
Dim strFind     As String
Dim longFind    As Long
Dim strStatus   As String

Sub Main()
    
    With Application
        .Calculation = xlCalculationManual
        .ScreenUpdating = False
    End With
    
    strFind = InputBox("String to find in formulas:", _
                       "Convert cells with x into values", _
                       "SUM")

    Call Convert2Values
    MsgBox "Done!"
    
    With Application
        .Calculation = xlCalculationAutomatic
        .ScreenUpdating = True
    End With
    
End Sub

Private Sub Convert2Values()
    
    For Each sht In ActiveWorkbook.Worksheets
        sht.Activate
        longFind = _
                    Application.CountA(sht.UsedRange, strFind)
                    
        If longFind > 0 Then
            strStatus = "Converting to values " & sht.Name
            Call UpdateStatusBar(strStatus)
            For Each rg In sht.UsedRange
                If InStr(1, rg.Formula, strFind) <> 0 Then
                    rg.Value = rg.Value
                End If
            Next rg
        End If
        
    Next sht

End Sub

Private Sub UpdateStatusBar(strStatus As String)
    
    Application.StatusBar = strStatus
    
End Sub
